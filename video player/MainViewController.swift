//
//  Test.swift
//  video player
//
//  Created by Umair Panwar on 18/07/2019.
//  Copyright © 2019 Umair Panwar. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices

class MainViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var imageView1: UIImageView!
    let imagePicker = UIImagePickerController()
    var url1:URL?=nil
    var url2:URL?=nil
    
    var clickedView:Int! // 1 for imageView1 and 2 for imageView2
    
    
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet var view1: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView2.tag=0x78
        imageView1.tag=0x67
        
        imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClick(_:))))
        imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClick(_:))))
        imageView2.isUserInteractionEnabled=true
        imageView1.isUserInteractionEnabled=true
    }
    
    @objc func onClick(_ sender : UITapGestureRecognizer){
        if sender.view!.tag == imageView2.tag{
            print("clicked on second image view")
        }
        let alert:UIAlertController=UIAlertController(title: "Choose Video",
                                                      message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let gallaryAction = UIAlertAction(title: NSLocalizedString("gallery", comment: ""),
                                          style: UIAlertAction.Style.default) {
                                            UIAlertAction in
                                            self.openCamera(UIImagePickerController.SourceType.photoLibrary)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                         style: UIAlertAction.Style.cancel) {
                                            UIAlertAction in
        }
        
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        alert.popoverPresentationController?.sourceView=sender.view!
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.openCamera(UIImagePickerController.SourceType.photoLibrary)
        if(sender.view!.tag == imageView1.tag){
            clickedView = 1
        }
        else if(sender.view!.tag == imageView2.tag){
            clickedView = 2
        }
        else {
            clickedView = 0
        }
        
    }
    
    func openCamera(_ sourceType: UIImagePickerController.SourceType) {
        imagePicker.sourceType = sourceType
        imagePicker.mediaTypes=[kUTTypeMovie as String]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let videoPaath = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        imagePicker.dismiss(animated: true, completion: nil)
        if(clickedView == 1){
            self.url1=videoPaath
        }
        else if(clickedView == 2){
            self.url2=videoPaath
        }
    }
    
    private func playVideo(_ url:URL,_ view:UIView){
        
        let player=AVPlayer(url: url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        playerController.view.frame=view.bounds
        playerController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        playerController.videoGravity = .resizeAspectFill
        self.addChild(playerController)
        view.addSubview(playerController.view)
        playerController.didMove(toParent: self)
        playerController.player?.play()
        
    }
    
    func showAlert(){
        let alert=UIAlertController(title: "Alert",
                                    message: "Please select both videos", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: { (_) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btn(_ sender: UIButton) {
        if url1 != nil && url2 != nil{
            playVideo(url1!, view1)
            playVideo(url2!, view2)
            btnTap.isHidden=true
        }
        if url1 == nil || url2 == nil{
            showAlert()
        }
    }
}
